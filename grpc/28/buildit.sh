#!/bin/bash

docker build -t grpc . $DOCKER_BUILD_ARGS
docker run -v "`pwd`/../../build:/build" grpc
