#!/bin/bash

docker build -t grpc-web . $DOCKER_BUILD_ARGS
docker run -v "`pwd`/../../build:/build" grpc-web
