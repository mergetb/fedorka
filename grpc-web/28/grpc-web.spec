Name:	    grpc-web
Version:  0.4.0
Release:	1%{?dist}
Summary:	A javascript/web plugin for GRPC

Patch0: install.patch

License:  Apache-2.0
URL:	    https://grpc.io	

BuildRequires: cmake gcc-c++ protobuf-devel	protobuf-compiler
Requires:	protobuf-c

%description
A javascript/web plugin that generates javascript client and server
GRPC modules and data structures.

%prep
if [[ ! -d grpc-web-0.4.0 ]]; then
  git clone -b 0.4.0 https://github.com/grpc/grpc-web grpc-web-0.4.0
fi
cd grpc-web-0.4.0
%patch0 -p1 -b .install

%build
cd grpc-web-0.4.0
export CC=clang
export CXX=clang++
make %{?_smp_mflags} plugin

%install
cd grpc-web-0.4.0
export prefix=%{buildroot}/usr
make install-plugin

%files
/usr/bin/protoc-gen-grpc-web

%changelog
* Tue Sep 4 2018 Ryan Goodfellow <rgoodfel@isi.edu> - 0.4.0-0
- Initial
